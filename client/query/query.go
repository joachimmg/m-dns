package query

import (
	"git.giaever.org/joachimmg/m-dns/errors"
	"git.giaever.org/joachimmg/m-dns/host"
	"git.git.giaever.org/joachimmg/m-dns/query/params"
)

type Query struct {
	params.Params

	unicast
	timeout   time.Duration
	instances chan<- *host.Host
}

func New(p params.Params, unicast bool, timeout int) (Query, error) {
	q := new(Query)

	if q == nil {
		log.Traceln(errors.Client, errors.OutOfMemory)
		return nil, errors.OutOfMemory
	}

	q.Params = p
	q.unicast = u
	q.timeout = t

	return q, nil
}

func NewDefault(service, domain string) (Query, error) {
	p, err := params.New(service, domain, nil)

	if err != nil {
		return nil, err
	}

	return New(p, config.ForceUnicast, config.QueryTimeout)
}
