package params

type Params struct {
	*net.Interface

	Service host.Service
	Domain  host.Domain
}

func New(service, domain string, iface *net.Interface) (*Params, error) {
	p := new(Params)

	if p.service, err = host.String(service).IsServiceVariable(); err != nil {
		log.Traceln(errors.Params, service, err)
		return nil, err
	}

	if p.domain, err = host.String(domain).IsDomainVariable(); err != nil {
		log.Traceln(errors.Params, domain, err)
		return nil, err
	}

	return p, nil
}
