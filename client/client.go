package client

import (
	"git.giaever.org/joachimmg/go-log.git/log"
	"git.giaever.org/joachimmg/m-dns/config"
	"git.giaever.org/joachimmg/m-dns/connection"
	"git.giaever.org/joachimmg/m-dns/errors"
	"git.giaever.org/joachimmg/m-dns/host"
)

type Client interface {
	Close() error
	Lookup(service, domain string, instances chan<- *host.Host) error
}

type client struct {
	ipv4u connection.UDP
	ipv6u connection.UDP
	ipv4m connection.UDP
	ipv6m connection.UDP

	running bool
	runCh   chan struct{}
}

func New() (Client, error) {

	c := new(client)

	if c == nil {
		log.Traceln(errors.Client, errors.OutOfMemory)
		return nil, errors.OutOfMemory
	}

	c.ipv4u = connection.New(4)
	c.ipv6u = connection.New(6)

	if err := c.ipv4u.Listen(config.ZeroIPv4Addr); err != nil {
		log.Traceln(errors.Client, config.ZeroIPv4Addr, err)
	}

	if err := c.ipv6u.Listen(config.ZeroIPv6Addr); err != nil {
		log.Traceln(errors.Client, config.ZeroIPv6Addr, err)
	}

	if !c.ipv4u.Listening() && !c.ipv6u.Listening() {
		log.Traceln(errors.Client, errors.ClientUDPuFailed, config.ZeroIPv4Addr, config.ZeroIPv6Addr)
		return nil, errors.ClientUDPuFailed
	}

	c.ipv4m = connection.New(4)
	c.ipv6m = connection.New(6)

	if err := c.ipv4m.ListenMulticast(nil, config.MdnsIPv4Addr); err != nil {
		log.Traceln(errors.Client, config.MdnsIPv4Addr, err)
	}

	if err := c.ipv6m.ListenMulticast(nil, config.MdnsIPv6Addr); err != nil {
		log.Traceln(errors.Client, config.MdnsIPv6Addr, err)
	}

	if !c.ipv4m.Listening() && !c.ipv6m.Listening() {
		log.Traceln(errors.Client, errors.ClientUDPmFailed, config.MdnsIPv4Addr, config.MdnsIPv6Addr)
		return nil, errors.ClientUDPmFailed
	}

	return c, nil
}

func (c *client) Close() error {

	if !c.running {
		return nil
	}

	log.Traceln(errors.Client, "Closing")
	c.ipv4u.Close()
	c.ipv6u.Close()

	c.ipv4m.Close()
	c.ipv6m.Close()

	c.running = nil
	return nil
}

func (c *client) Lookup(service, domain string, instances chan<- *host.Host) error {
	return nil
}
