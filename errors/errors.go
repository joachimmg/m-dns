package errors

import (
	e "errors"
)

type Prefix string

const (
	Zone       Prefix = "Zone"
	Host       Prefix = "Host"
	HostString Prefix = "HostString"
	HostIP     Prefix = "HostIP"
	HostPort   Prefix = "HostPort"
	Server     Prefix = "Server"
	Client     Prefix = "Client"
	Query      Prefix = "Query"
	Params     Prefix = "Params"
)

var (
	OutOfMemory = e.New("Out of memory")

	ClientUDPuFailed = e.New("Failed to bind to any unicast address.")
	ClientUDPmFailed = e.New("Failed to bind to any multicast address.")

	HostIsNil   = e.New("Host is nil.")
	ServerIsNil = e.New("Server is nil.")

	HostStringIsEmpty           = e.New("String is empty.")
	HostStringIsInvalid         = e.New("String is invalid.")
	HostStringIsInvalidInstance = e.New("String is not an instance, e.g <Readable description>.")
	HostStringIsInvalidService  = e.New("String is not a service, e.g <_service._tcp> or <_service._udp>.")
	HostStringIsInvalidDomain   = e.New("String is no a valid domain, e.g <local> or <my.domain>.")
	HostStringIsInvalidHostname = e.New("Hostname is invalid.")

	HostIPIsInvalid = e.New("IP address(es) is not valid for hostname.")

	HostPortIsInvalid = e.New("Port number is invalid (p < 0 || p > 65535).")

	HostTXTExceedsLimit = e.New("TXT record exceed intended size -- 200 bytes or less.")

	ServerNoListenersStarted     = e.New("No multicast listeners started.")
	ServerReceivedNonQueryOpcode = e.New("Received non-query Opcode")
	ServerReceivedNonZeroRcode   = e.New("Received non-zero Rcode")
	ServerReceivedTruncatedSet   = e.New("Reveived trucated bit set")
	ServerNoResponseForQuestion  = e.New("No response for question.")
	ServerUnknownConnectionAddr  = e.New("Unknown connection on IP.")
)

func (p Prefix) String() string {
	return string(p) + ":"
}
