package zone

import (
	"net"
	"os"
	"testing"

	"git.giaever.org/joachimmg/m-dns/host"
)

func TestZoneInit(t *testing.T) {

	hostname, err := os.Hostname()

	if err != nil {
		t.Fatal(err)
	}

	txt := []string{
		"=ignore",
		"key=value pair",
		"key=",
		"key   =   value=pair",
		"k  e  y = v l a e u",
		"--key",
	}

	host, err := host.New(
		"This is info about \\my own service.",
		"_http._tcp",
		"local",
		hostname,
		[]net.IP{},
		8001,
		txt,
	)

	if err != nil {
		t.Fatal(err)
	}

	zone, err := New(host)

	if err != nil {
		t.Fatal(err)
	}

	t.Log(zone)
	t.Fail()
}
