package server

import (
	"net"
	"os"
	"testing"

	"git.giaever.org/joachimmg/go-log.git/log"
	"git.giaever.org/joachimmg/m-dns/host"
	"git.giaever.org/joachimmg/m-dns/zone"
)

func TestServerInit(t *testing.T) {
	hostname, err := os.Hostname()

	if err != nil {
		t.Fatal(err)
	}

	txt := []string{
		"=ignore",
		"key=value pair",
		"key=",
		"key   =   value=pair",
		"k  e  y = v l a e u",
		"--key",
	}

	host, err := host.New(
		"This is info about \\my own service.",
		"_http._tcp",
		"local",
		hostname,
		[]net.IP{},
		8001,
		txt,
	)

	if err != nil {
		t.Fatal(err)
	}

	zone, err := zone.New(host)

	if err != nil {
		t.Fatal(err)
	}

	log.Traceln(zone)

	mdnss, err := New(zone, nil)

	if err != nil {
		t.Fatal(err)
	}
	t.Log(mdnss)
	mdnss.Daemon()
	t.Fail()
}
