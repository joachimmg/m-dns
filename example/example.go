package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"

	"git.giaever.org/joachimmg/go-log.git/log"
	"git.giaever.org/joachimmg/m-dns/client"
	"git.giaever.org/joachimmg/m-dns/host"
	"git.giaever.org/joachimmg/m-dns/server"
	"git.giaever.org/joachimmg/m-dns/zone"
)

var runServer bool

func init() {
	flag.BoolVar(&runServer, "s", false, "Server if set to true, client otherwise.")
	flag.Parse()
}

func main() {
	hostname, err := os.Hostname()

	if err != nil {
		log.Errorln(err)
	}

	if runServer {
		txt := []string{
			"login=true",
			"admin=/admin",
			"autosign=",
		}

		host, err := host.New(
			"This is _a_ .dotted. instance",
			"_myservice._tcp",
			"local",
			hostname,
			[]net.IP{net.ParseIP("192.168.1.128"), net.IPv4zero},
			8080,
			txt,
		)

		if err != nil {
			log.Errorln(err)
		}

		zone, err := zone.New(host)

		if err != nil {
			log.Errorln(err)
		}

		mdns, err := server.New(zone, nil)

		if err != nil {
			log.Errorln(err)
		}
		log.Traceln(mdns)

		defer mdns.Close()
		//mdnss.Daemon()

		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Hello, I'm just here hangig around.")
		})

		log.Panicln(http.ListenAndServe(":8080", nil))
	} else {
		mdns, err := client.New()

		if err != nil {
			log.Panicln(err)
		}

		defer mdns.Close()

		log.Traceln(mdns)
	}
}
