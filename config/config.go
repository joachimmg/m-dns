package config

import (
	"net"
)

const (
	MdnsIPv4 = "224.0.0.251"
	MdnsIPv6 = "ff02::fb"
	MdnsPort = 5353

	ForceUnicast = false

	DefaultTTL = 120

	BufSize          = 65536
	BufReadDeadline  = 2
	BufWriteDeadline = 2

	QueryTimeout = 1
)

var (
	MdnsIPv4Addr = &net.UDPAddr{
		IP:   net.ParseIP(MdnsIPv4),
		Port: MdnsPort,
	}
	MdnsIPv6Addr = &net.UDPAddr{
		IP:   net.ParseIP(MdnsIPv6),
		Port: MdnsPort,
	}

	ZeroIPv4Addr = &net.UDPAddr{
		IP:   net.IPv4zero,
		Port: 0,
	}

	ZeroIPv6Addr = &net.UDPAddr{
		IP:   net.IPv6zero,
		Port: 0,
	}
)
