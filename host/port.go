package host

import (
	"git.giaever.org/joachimmg/go-log.git/log"
	"git.giaever.org/joachimmg/m-dns/errors"
)

type HostPort interface {
	Int() int
	Uint16() uint16
}

type Port int

func (p Port) Int() int {
	return int(p)
}

func (p Port) Uint16() uint16 {
	return uint16(p)
}

func (p Port) isValid() error {
	log.Traceln(errors.HostPort, p)
	if int(p) < 0 || int(p) > 65535 {
		return errors.HostPortIsInvalid
	}

	return nil
}
